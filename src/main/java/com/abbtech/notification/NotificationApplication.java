package com.abbtech.notification;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.kafka.annotation.EnableKafka;

@SpringBootApplication
@EnableFeignClients
@RequiredArgsConstructor
@EnableKafka
public class NotificationApplication{




	public static void main(String[] args) {
		SpringApplication.run(NotificationApplication.class, args);
	}



}
