package com.abbtech.notification.service.impl;

import java.util.UUID;
import com.abbtech.notification.dto.NotificationDto;
import com.abbtech.notification.service.NotificationService;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class NotificationServiceImpl implements NotificationService {

    @KafkaListener(topics = "order_confirmed", groupId = "kafka-group-id")
    public void listen(NotificationDto notificationDto){
        System.out.println(notificationDto.toString()+" this order is confirmed");
    }
}
