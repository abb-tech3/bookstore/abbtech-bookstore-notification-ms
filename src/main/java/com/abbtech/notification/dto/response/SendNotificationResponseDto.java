package com.abbtech.notification.dto.response;


import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SendNotificationResponseDto {
    private UUID id;
    private String message;
    private String userEmail;
    private UUID userId;
    private UUID orderId;
}
