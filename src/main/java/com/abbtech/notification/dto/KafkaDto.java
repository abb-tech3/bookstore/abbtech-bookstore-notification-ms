package com.abbtech.notification.dto;

import lombok.Data;

@Data
public class KafkaDto {
    private int id;
    private String name;
}
