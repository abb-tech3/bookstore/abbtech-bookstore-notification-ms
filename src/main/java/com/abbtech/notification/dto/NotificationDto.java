package com.abbtech.notification.dto;


import java.util.UUID;
import lombok.Data;

@Data
public class NotificationDto {
    private UUID orderID;
    private UUID userId;
    private String message;
}
